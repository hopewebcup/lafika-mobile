import 'react-native-gesture-handler';
import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';

type Props = {
  children: React.ReactChildren;
};

export default function NavigationProvider(props: Props) {
  return <NavigationContainer>{props.children}</NavigationContainer>;
}
