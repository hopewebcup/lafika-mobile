/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Animated,
  Easing,
} from 'react-native';
import LottieView from 'lottie-react-native';

import {Colors} from 'react-native/Libraries/NewAppScreen';

import {LoginButton, AccessToken, LoginResult} from 'react-native-fbsdk';

declare const global: {HermesInternal: null | {}};

const App = () => {
  const [progress] = useState(new Animated.Value(0));

  const onLoginFinished = (error: object, result: LoginResult) => {
    if (error) {
      console.log('login has error: ' + result.error);
    } else if (result.isCancelled) {
      console.log('login is cancelled.');
    } else {
      AccessToken.getCurrentAccessToken().then((data) => {
        if (data) {
          console.log(data.accessToken.toString());
        }
      });
    }
  };

  const onLogoutFinished = () => {
    console.log('logout.');
  };

  useEffect(() => {
    Animated.timing(progress, {
      toValue: 1,
      duration: 5000,
      easing: Easing.linear,
    } as Animated.TimingAnimationConfig).start();
    return () => {};
  });

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
          {global.HermesInternal == null ? null : (
            <View style={styles.engine}>
              <Text style={styles.footer}>Engine: Hermes</Text>
            </View>
          )}
          <View style={styles.body}>
            <View style={styles.sectionContainer}>
              <Text style={styles.sectionTitle}>Step One</Text>
            </View>
          </View>
          <View style={styles.footer}>
            <LottieView
              source={require('./src/assets/animation/doctor_virtual.json')}
              progress={progress}
            />
            <LoginButton
              onLoginFinished={onLoginFinished}
              onLogoutFinished={onLogoutFinished}
              style={styles.loginButton}
            />
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    textAlign: 'right',
    height: 200,
  },
  loginButton: {
    height: 50,
    margin: 20,
    position: 'relative',
    bottom: 20,
  },
});

export default App;
